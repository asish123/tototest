<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    DB::table('todos') -> insert(
        [
            ['title' => 'todo sport',
            'user_id'=>1 ,
            'created_at'=> date('Y-m-d G:i:s'),
            'status' => '0',
        ],
        ['title' => 'todo sport2',
            'user_id'=>2 ,
            'created_at'=> date('Y-m-d G:i:s'),
            'status' => '0',
    ],
        ]);
    }

}
