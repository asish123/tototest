<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users') -> insert(
            [
                ['name' => 'reut',
                'email' => 'reut@gmail.com',
                'password' => Hash::make('12345678'),
                'created_at'=> date('Y-m-d G:i:s'),
            ],
            ['name' => 'ori',
            'email' => 'ori@gmail.com',
            'password' => Hash::make('123456789'),
            'created_at'=> date('Y-m-d G:i:s'),
           
            ],
            ]);
            
    }
}
