<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    public function user(){
        return $this -> belongsTo('App\User');
    }
    // מאפשרים למשתמש לערוך את השדות טייטל וסטאטוס
    protected $fillable = [
                'title','status' ];
            
}
